//Setting up TOKEN for ajax requests
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

//ADD NEW PARENT FUNCTIONALITY----------->
function addNewParent(obj){
    return $.ajax({
        url: 'parent/add',
        data: obj,
        method: 'POST'
    })

}

//REMOVE PARENT FUNCTIONALITY----------->
function removeParent(obj){
    return $.ajax({
        url: 'parent/remove',
        method: 'POST',
        data:obj
    })
}

//UPDATE HALL FUNCTIONALITY----------->
function updateParent(obj){
    return $.ajax({
        url: 'parent/update',
        data:obj,
        method: 'POST'
    })
}

//ADD NEW CHILD FUNCTIONALITY----------->
function addNewChild(obj){
    return $.ajax({
        url: 'child/add',
        data: obj,
        method: 'POST'
    })

}

//REMOVE CHILD FUNCTIONALITY----------->
function removeChild(obj){
    return $.ajax({
        url: 'child/remove',
        method: 'POST',
        data:obj
    })
}

//UPDATE CHILD FUNCTIONALITY----------->
function updateChild(obj){
    return $.ajax({
        url: 'child/update',
        data:obj,
        method: 'POST'
    })
}


//GET CHILDREN FOR CURRENT PARENT FUNCTIONALITY----------->
function getChildrenForParent(obj){
    return $.ajax({
        url: 'children/get',
        data:obj,
        method: 'GET'
    })
}

//GET ORPHANS FUNCTIONALITY----------->
function getOrphans(){
    return $.ajax({
        url: 'orphans/get',
        method: 'GET'
    })
}

