function capitalize(str){
	if(str!=undefined)
	return str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	  			return letter.toUpperCase();
	});
}


 function liftMeUp(offset,duration){
 	$('html, body').animate({scrollTop: offset}, duration);
 }




function createSelectYear(){

	for (i = new Date().getFullYear(); i > 1900; i--){
    	$('select[name="start"],select[name="end"]').append($('<option />').val(i).html(i));
	}
}

//SETTING DEFAULTS FOR ALL LINKS
function setDefaults(){
	$('main a,footer a').click(function (e) {
	    e.preventDefault();
	    e.stopPropagation();
	});
}


createSelectYear();
setDefaults();
