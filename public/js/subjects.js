var addParentForm=$('.parentAddForm'),            //the form to add new parents to the parents table
addParentInput=addParentForm.find('input'),       //input for adding the parents
parentName=$('.parentName'),                      //parent name links in the parents table
childsTable=$('#childsTable'),                    //children table
parentsTable=$('#parentsTable'),                  //parents table
flash_msg=$('#flash_msg'),                        //flash messages element
addChildBtn= $('.addChild')                       //add new child button


//SENDING AJAX REQUEST TO LOAD ALL JS FUNCTIONALITY AT ONCE
$.get('',function(data){
    refreshParentsList(data)
})


//***********************************PARENT ACTIONS**********************************************************************/
//Toggles add-parent form
$('.addParent').click(function(){
   addParentForm.toggleClass('hidden')
   addParentInput.val('');          //emptying the input after every toggle
})

//ADD NEW PARENT FUNCTIONALITY
function addParent(input,e){

    //detects 'enter' button clicking
    if(e.which==13){
        e.preventDefault();
        val=input.val();


        //calling to the appropriate service for adding new parent
        addNewParent({parentName:val,parentId:0}).success(function(data){

            //refreshing parents table to show newly added element
            refreshParentsList(data);

            //showing flash message for added new parent element
            flash_msg.html('The parent item <strong>'+val+'</strong> has been added successfully').fadeIn().delay(2000).fadeOut();
            addParentForm.addClass('hidden'); //hiding parent form if everything is ok

        //this left for error messages
        }).error(function(data){
            var data=JSON.parse(data.responseText),
            error=data.parentName?data.parentName:'There was an error while adding <strong>'+val+'</strong> item '; //setting error message

            //showing flash error message for the current element adding
            flash_msg.removeClass('alert-success').addClass('alert-danger').html(error).fadeIn().delay(2000).fadeOut();
        })
    }
}

//REMOVE PARENT FUNCTIONALITY
function removeParentAction(button){

    //getting appropriate parameters for removing parent
    parentId=button.attr('data-parent-id');
    id=button.attr('data-id');
    name=button.parent().siblings('.parentTitle').find('.parentName').text();       //parent name

    msg=confirm("Are you sure you want to remove this parent item?");               //confirmation box with its message

    //if user confirms the parent removal proceeding to appropriate service
    if(msg){
        removeParent({id:id,parentId:parentId}).success(function(data){

            //refreshing parents table to show to show the result after removing the selected parent element
            refreshParentsList(data)

            //showing successfully-removed information for the selected parent element
            flash_msg.html('The parent item  <strong>'+name+'</strong> has been deleted successfully').fadeIn().delay(2000).fadeOut();
        }).error(function(){

            //showing flash error message for the current element removing
            flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while removing <strong>'+name+'</strong> item ').fadeIn().delay(2000).fadeOut();

        })
    }

}

//EDIT PARENT FUNCTIONALITY
function editParent(button){

    //getting necessary parameters for removing parent
    var id=button.attr('data-id'),
    parentId=button.attr('data-parent-id'),
    cell=button.parent().siblings('td');        //current editing cell

    //hiding input for editing current parent name and showing all parent names if there is a hidden one
    $('.parentVal').addClass('hidden')
    parentsTable.find('a').removeClass('hidden')

    //showing input for editing current parent name and hiding current parent name
    nameEl=cell.find('a').addClass('hidden');
    parentInput=cell.find('.parentVal').removeClass('hidden');


    //sending parent parameters to appropriate service on 'enter' button click
    parentInput.keydown(function(e){

        if(e.which==13) {
            val=$(this).val();
            updateParent({parentId:parentId,parentName:val,id:id}).success(function(){
                nameEl.text(val).removeClass('hidden');     //changing parent name in the current cell and showing it
                parentInput.addClass('hidden')              //hiding parent input for current cell

                //showing updated-successfully message and hiding after a while
                flash_msg.html('The parent item  <strong>'+val+'</strong> has been updated successfully').fadeIn().delay(2000).fadeOut();
            }).error(function(){

                //showing a flash error message for the current element adding
                flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while updating <strong>'+val+'</strong> item ').fadeIn().delay(2000).fadeOut();

            })
        }
    })
}


//REFRESH PARENTS TABLE
function refreshParentsList(data){

    data=JSON.parse(data);                                      //parsing the received JSON-formatted data
    parentsTable.html('')                                       //emptying parents table
    for(i=0;i<data.length;i++){
        parentsTable.append(getParentLayout(data,i))            //appending parents table layout with the received data
    }

    //Adding parent
    addParentInput.unbind().keydown(function(e){
        addParent($(this),e);
    })

    //Deleting parent
    $('.removeParent').unbind().click(function(){
        removeParentAction($(this));
    })

    //Editing parent
    $('.editParent').unbind().click(function(){
        editParent($(this))
    })

    //Load children for current parent
    $('.parentName').unbind().click(function(e){
        e.preventDefault();
        e.stopPropagation();
        loadChildrenData($(this));
    })

}

//RETURN THE PARENT TABLE LAYOUT
function getParentLayout(data,i){
    parent=data[i];
    return '<tr>\
                <td>'+(i+1)+'</td>\
                <td class="parentTitle">\
                    <a href="#" class="parentName" data-parent-id="'+parent.Id+'">'+parent.Name+'</a>\
                    <input class="parentVal hidden" value="'+parent.Name+'">\
                </td>\
                <td>\
                    <button class="btn btn-warning editParent" data-parent-id="'+parent.ParentId+'" data-id="'+parent.Id+'">Edit</button>\
                    <button class="btn btn-danger removeParent" data-id="'+parent.Id+'" data-parent-id="'+parent.ParentId+'">Delete</button>\
                </td>\
            </tr>'
}


//***********************************************CHILDREN ACTIONS******************************************************/

//REFRESH CHILDREN TABLE
function refreshChildrenList(data){

    data=JSON.parse(data);                                    //parsing the received JSON-formatted data
    childsTable.html('')                                      //emptying children table
    for(i=0;i<data['children'].length;i++){
        childsTable.append(getChildLayout(data,i))            //appending children table layout with the received data
    }


    //Adding child
    $('.childAddForm').unbind().keydown(function(e){
        if(e.which==13) {
            e.preventDefault();
            addChild($(this), e);
        }
    })

    //Deleting parent
    $('.removeChild').unbind().click(function(){
        removeChildAction($(this))
    })

    //Editing parent
    $('.editChild').unbind().click(function(){
        editChildAction($(this))

    })
}

//RETURN THE CHILDREN TABLE LAYOUT
function getChildLayout(data,i){
    children=data['children'][i];
    return '<tr>\
                <td>'+(i+1)+'</td>\
                 <td class="childName">\
                    <span>'+children.Name+'</span>\
                    <input class="childVal hidden" value="'+children.Name+'"><br>\
                    <select class="parentsDropdown hidden">'+createParentDropdown(data)+'</select>\
                 </td>\
                 <td>\
                    <button class="btn btn-warning editChild" data-parent-id="'+children.ParentId+'" data-id="'+children.Id+'">Edit</button>\
                    <button class="btn btn-danger removeChild" data-id="'+children.Id+'" data-parent-id="'+children.ParentId+'">Delete</button>\
                    <button class="btn btn-success saveChild hidden" data-id="'+children.Id+'" data-parent-id="'+children.ParentId+'">Save</button>\
                    <button class="btn btn-danger cancelSaveChild hidden" data-id="'+children.Id+'" data-parent-id="'+children.ParentId+'">Cancel</button>\
                </td>\
            </tr>'
}

//CREATE PARENT DROPDOWN IN EDIT-CHILDREN FUNCTIONALITY 
function createParentDropdown(dt){

    opts='';
    if(dt["parents"]){
        for(j=0;j<dt["parents"].length;j++){
           opts+='<option value="'+dt["parents"][j].Id+'" '
           +(dt["parents"][j].Id==dt['children'][i].ParentId?'selected':'')+'>'
           +dt["parents"][j].Name+'</option>';     
        }
    }
    
    return opts;
}

//Load children for current parent
parentName.unbind().click(function(e){
    e.preventDefault();
    e.stopPropagation();
    loadChildrenData($(this));
})

//LOAD CHILDREN INSIDE CHILDREN TABLE
function loadChildrenData(item){

    $('.selectedParent').removeClass('selectedParent');                 //resetting selected parents
    item.addClass('selectedParent');                                    //marking the current selected parent as should
    parentId=item.attr('data-parent-id')                                //getting the parent id of the selected parent


    //sending the parameters of the current selected parent
    getChildrenForParent({parentId:parentId}).success(function(data){

        childsTable.removeClass('hidden').html('')                      //resetting children table and showing it
        addChildBtn.removeClass('hidden')                               //showing add-child button
        refreshChildrenList(data);                                      //refreshing children table

        //getting parameters for child-adding form
        addChildForm=$('.childAddForm');
        addChildInput=addChildForm.find('input')

        //Toggles add-parent form
        addChildBtn.unbind().click(function(){
            addChildForm.toggleClass('hidden')
            addChildInput.val('');
        })


        //Adding child
        addChildInput.unbind().keydown(function(e){
            if(e.which==13) {
                e.preventDefault();
                addChild($(this), e);
            }
        })



        //Deleting parent
        $('.removeChild').unbind().click(function(){
            removeChildAction($(this))
        })

        //Editing parent
        $('.editChild').unbind().click(function(){
            editChildAction($(this))

        })

    }).error(function(){

        //showing a flash error message for the loading of child elements
        flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while loading child elements for <strong>'+item.text()+'</strong> parent item ').fadeIn().delay(2000).fadeOut();

    })
}

//ADD CHILD FUNCTIONALITY
function addChild(input){

    val=input.val();                        //getting the value of child that has to be created

    if(val){

        addChildForm.addClass('hidden')     //hiding add-child form

        //sending child name and its parent id to the appropriate service
        addNewChild({childName:val,parentId:$('.selectedParent').attr('data-parent-id')}).success(function(data){

            refreshChildrenList(data)       //refreshing child table with the new data

            //showing completed-successfully message for adding new child
            flash_msg.html('The child item  <strong>'+val+'</strong> has been added successfully').fadeIn().delay(2000).fadeOut();
        }).error(function(){

            //showing a flash error message for the loading of child elements
            flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while adding <strong>'+val+'</strong> child element').fadeIn().delay(2000).fadeOut();
        })

    }
}

//REMOVE CHILD FUNCTIONALITY
function removeChildAction(button){

    //getting necessary parameters for child element removal
    parentId=button.attr('data-parent-id');
    id=button.attr('data-id');
    name=button.parent().siblings('.childTitle').find('.childName').text();

    //showing confirmation box for child element removal
    msg=confirm("Are you sure you want to remove this child item?");

    //if user confirms child element removal, sending the parameters to the appropriate service
    if(msg){
        removeChild({id:id,parentId:parentId}).success(function(data){

            refreshChildrenList(data)           //refreshing child table with the result

            //showing completed-successfully message for removing the current child element
            flash_msg.html('The child item  <strong>'+name+'</strong> has been removed successfully').fadeIn().delay(2000).fadeOut();

        }).error(function(){

            //showing flash error message for the current element removing
            flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while removing <strong>'+name+'</strong> item ').fadeIn().delay(2000).fadeOut();

        })
    }

}

//EDIT CHILD FUNCTIONALITY
function editChildAction(button){

    //getting necessary parameters for child element editing
    parentId=button.attr('data-parent-id');
    id=button.attr('data-id');
    cell=button.parent().siblings('td');
    saveButton=button.siblings('.saveChild');
    cancelButton=button.siblings('.cancelSaveChild');
   


    //hiding input for editing current child name and showing all child names if there is a hidden one
    $('.childVal,.parentsDropdown').addClass('hidden')
    childsTable.find('span').removeClass('hidden')


    //showing input for editing current child name,save and cancel buttons and hiding current child name
    nameSpan=cell.find('span').addClass('hidden');
    childInput=cell.find('.childVal').removeClass('hidden');
    parentsDropdown=childInput.siblings('.parentsDropdown').removeClass('hidden');
    button.siblings('.saveChild,.cancelSaveChild').removeClass('hidden')
    button.addClass('hidden')
    button.siblings('.removeChild').addClass('hidden')
    
   

    //updating only child name on enter button click
    childInput.keydown(function(e){
        if(e.which==13) {
            parentId=parentsDropdown.val();
            val=$(this).val();
            
            toUpdateChildService(parentId,id,val)    
           
        }
    })

    //updating all on save button click
    saveButton.click(function(){
        parentId=parentsDropdown.val();
        toUpdateChildService(parentId,id,childInput.val())
    })

    //cancelling all edit actions for the current child
    cancelButton.click(function(){
        nameSpan.removeClass('hidden');                //changing child name in the current cell and showing it
        childInput.addClass('hidden')                  //hiding child input for current cell
        parentsDropdown.addClass('hidden')             //hiding current child parent of the dropdown
        $(this).addClass('hidden')
        saveButton.addClass('hidden') 
        button.removeClass('hidden')
        button.siblings('.removeChild').removeClass('hidden')
    })

    

}

//SENDING EDITTED DATA TO UPDATE-CHILD SERVICE
function toUpdateChildService(parentId,id,val){
     //sending parent parameters to appropriate service on 'enter' button click
     updateChild({parentId:parentId,id:id,childName:val}).success(function(data){

        nameSpan.text(val).removeClass('hidden');      //changing child name in the current cell and showing it
        childInput.addClass('hidden')                  //hiding child input for current cell
        parentsDropdown.addClass('hidden')             //hiding current child parent of the dropdown 

        refreshChildrenList(data)                      //refreshing child table with the new data

        //showing completed-successfully message for updated name of current child element
        flash_msg.html('The child item  <strong>'+val+'</strong> has been updated successfully').fadeIn().delay(2000).fadeOut();

    }).error(function(){

        //showing flash error message for the current element updating
        flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while updating <strong>'+name+'</strong> item ').fadeIn().delay(2000).fadeOut();
    })
}






//***********************************************ORPHANS FUNCTIONALITY*************************************************/
$('.showOrphans').click(function(){

    //getting orphans elements from the appropriate service
    getOrphans().success(function (data) {

        childsTable.removeClass('hidden')                       //showing children table
        addChildBtn.addClass('hidden')                          //hiding add-child button for orphans case

        refreshChildrenList(data)                               //refreshing child table with the new data
    }).error(function(){

        //showing a flash error message for the loading of child elements
        flash_msg.removeClass('alert-success').addClass('alert-danger').html('There was an error while loading orphan elements').fadeIn().delay(2000).fadeOut();
    })
})