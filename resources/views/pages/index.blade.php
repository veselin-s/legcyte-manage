@extends('app')



@section('content')
<div class="leftSide">
    <button class="btn btn-primary addParent" >Add parent</button>
    <button class="btn btn-primary showOrphans" >Orphans</button>
    <form class="parentAddForm hidden">
        <label>Parent name<input type="text" id="parentName"> </label>
    </form>
    <table  id="parentsTable" >

      <!--  @foreach($parents as $parent)
            <tr>
                <td>{{++$rowNumber}}</td>
                <td class="parentTitle">
                    <a href="#" class="parentName" data-parent-id="{{$parent->Id}}">{{$parent->Name}}</a>
                    <input class="parentVal hidden" value="{{$parent->Name}}">
                </td>
                <td>
                    <button class="btn btn-warning editParent" data-id="{{$parent->Id}}" data-parent-id="{{$parent->ParentId}}">Edit</button>
                    <button class="btn btn-danger removeParent" data-name="{{$parent->Name}}" data-id="{{$parent->Id}}" data-parent-id="{{$parent->ParentId}}">Delete</button>
                </td>
            </tr>

        @endforeach -->
    </table>
</div>


<div class="rightSide">
    <button class="btn btn-primary addChild hidden">Add child</button>
    <form class="childAddForm hidden">

        <label>Child name<input type="text" id="childName"></label>

    </form>
    <table  id="childsTable" class="hidden">

       <!-- @foreach($children as $child)
            <tr>
                <td>{{++$childRowNumber}}</td>
                <td class="childName">
                    <span>{{$child->Name}}</span>
                    <input class="childVal hidden" value="{{$child->Name}}">
                </td>
                <td>{{$child->ParentId}}</td>
                <td>
                    <button class="btn btn-warning editChild" data-parent-id="{{$child->ParentId}}" data-id="{{$child->Id}}">Edit</button>
                    <button class="btn btn-danger removeChild" data-id="{{$child->Id}}" data-parent-id="{{$child->ParentId}}">Delete</button>
                </td>
            </tr>

        @endforeach  -->
    </table>
</div>
@stop