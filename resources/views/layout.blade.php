<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf8">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<title>Films for today</title>
<link rel="shortcut icon" href="images/backgrounds/logo.ico"><!-- иконка данного сайта-->
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">-->
<link rel="stylesheet" href="styles/style.css" />
</head>

<body>
	<div id="container">

			<!--********************main content*****************************************-->
			<main>
				@yield('content')
				 <div class="subpage"></div>
			</main>

            @yield('footer')
            <!--***********************footer***************************************************-->
            <footer>
               <a href="#" id="back_to_top"><img src="images/buttons/back_to-top.png" width="99" height="17" /></a>
            </footer>

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        {!!HTML::script('js/jquery-ui.js')!!}
        {!!HTML::script('js/jquery.datepicker.js')!!}
            <script src="js/admin/services.js" defer="defer"></script>
			<script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.color.js" defer="defer"></script><!--анимация с цветами-->
            <script src="js/functions.js" defer="defer"></script>
            <script src="js/storage.js" defer="defer"></script>
            <script src="js/pages.js" defer="defer"></script><!--управление загрузкой страниц-->
            <script src="js/modal.js" defer="defer"></script>

		<script>
            $('input.datepicker').datepicker({showOtherMonths: true,dateFormat: 'dd/mm/yy',dayNamesMin: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']});
        </script>
	</div>



			
</body>
</html>



        
  


			

	
			
			

</html>