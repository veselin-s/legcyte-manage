<div  class="form-group" id="uploadImages">

   
    <input type="hidden" id="filmImg" name="image" value="">
   <!-- <input type="radio" name="directory" value="smalls" checked>small
    <!--<input type="radio" name="directory" value="bigs">big
    <input type="radio" name="directory" value="covers">cover-->

        <progress id="progressBar" value="0" max="100" class="hidden"></progress>
        <h3 id="status" class="hidden">OK</h3>


    @if(Session::has('folder_msg'))
        <div class="alert alert-success {{Session::has('flash_mesg_important')?'alert-important':''}}">
            {{session('folder_msg')}}
        </div>
    @endif
    <div id="dropzone" data-subdir="small">
         <input type="file" class="hidden" >
        <a class="createFolder hidden btn btn-default hidden">Create folder</a>
        <span class="folderMsg hidden">Folder exists</span>
        <p class="hidden">Move your files here</p>

        <ul class="workFiles">
            <li>
                <img class="coverImg" src="{{$path.$cover}}">
                <img class="closeImg" data-file="{{$cover}}" src="{{$path}}close2.png">
            </li>
        </ul>
    </div>

</div>
