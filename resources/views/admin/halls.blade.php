@extends('app')

@section('content')
    <button class="btn-success" style="border:none" id="addHall">Add Hall</button>
    <table border="1" id="hallsTable">
        <tr>
            <th>N</th>
            <th>Title</th>
            <th>Seats</th>
            <th>Actions</th>
        </tr>
    @foreach($halls as $hall)
        <tr>
            <td>{{$hall->hall_id}}</td>
            <td><span>{{$hall->title}}</span><input class="editTitle hidden" value="{{$hall->title}}"></td>
            <td><span>{{$hall->seats_num}}</span><input class="editSeatsNum hidden" value="{{$hall->seats_num}}"></td>
            <td>
                <button class="btn-alert editHall" data-id="{{$hall->hall_id}}" style="border:none" >Edit</button>
                <button class="btn-danger deleteHall" data-id="{{$hall->hall_id}}" style="border:none" >Delete</button>
            </td>
        </tr>
    @endforeach
    </table>
@stop