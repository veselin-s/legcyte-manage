
<div class="float-left">
		<div class="form-group">
			{!!Form::label('title','Title')!!}
			{!!Form::text('title',$film_title,['class'=>'form-control'])!!}
		</div>

		<div class="form-group">
			{!!Form::label('dateVal','Dates')!!}
			{!!Form::text('dateVal','',['class'=>'form-control datepicker'])!!}
			<p>{{$dateMsg}}</p>

		</div>

		


		<div class="form-group">
			{!!Form::label('hall_id','Hall')!!}



			<select name="hall_id">


					@foreach($halls as $hall)
						<option value="{{$hall->hall_id}}">{{$hall->title}}</option>
					@endforeach


			</select>

		</div>
		<div class="form-group" id="sessionsDiv" data-hall="{{(count($halls)>0)?$halls[0]->hall_id:''}}">
			{!!Form::label('sessions','Sessions')!!}


			<input type="hidden" name="sessions" value="{{$hallsArr}}">
			<ul class="sessionsList">

				@foreach($hours as $h)
					<li>
						<label>
							{!! Form::checkbox('hours',$h->hour,($h->taken!=0)?true:false,
							[
							'class'=>'hourVal',
							'data-id'=>$h->hour_id
							]) !!}
							<span>{{$h->hour}}</span>


						</label>
					</li>
				@endforeach
			</ul>
		</div>
</div>

<div class="float-right">
@include('admin.dropzone')
</div>

<div class="form-group" id="submitArea">
	{!!Form::submit($submitButtonText,['class'=>'btn btn-primary'])!!}
	<a class="back btn btn-default" href="{{url('films')}}">Back</a>
</div>













