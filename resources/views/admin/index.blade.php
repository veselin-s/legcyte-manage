
@extends('app')

@section('content')
{!!Form::open(['method'=>'GET','url'=>'films/add'])!!}
{!!Form::submit('Add Film',['class'=>'btn btn-success','id'=>'addFilm'])!!}
{!!Form::close()!!}

<table id="filmsTable">
<tr>
<th>Title</th>
<th>Action</th>
</tr>

@foreach($films as $film)
<tr>
<td>

{!! HTML::link('films/edit/'.$film->film_id,$film->title) !!}
</td>
<td>

{!!Form::open(['method'=>'GET','url'=>'films/remove/'.$film->film_id])!!}
{!!Form::hidden('folder',$film->folder)!!}
{!!Form::submit('Delete',['class'=>'btn btn-danger','id'=>'deleteWork'])!!}
{!!Form::close()!!}
</td>
</tr>
@endforeach
</table>


@stop

