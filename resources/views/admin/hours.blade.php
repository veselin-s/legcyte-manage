@extends('app')

@section('content')
    <button class="btn-success" style="border:none" id="addSessionHour">Add Session</button>
    <input id="addHour" class="hidden">
    <ul id="hoursList">
    @foreach($hours as $hour)
        <li>
            <span class="hourItem">{{$hour->hour}}</span>
            <input class="hourInput hidden" data-id="{{$hour->hour_id}}" value="{{$hour->hour}}">
            <button type='button' class='close' data-id="{{$hour->hour_id}}">×</button>
        </li>
    @endforeach
    </ul>
@stop