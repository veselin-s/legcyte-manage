@extends('app')

@section('content')

	{!!Form::open(['url'=>'films/store','files'=>true,'class'=>'filmsForm'])!!}

		<!--errors section-->
		@include('errors.list')

		<!--actual form-->
		@include('admin.form',['submitButtonText'=>'Add Film'])

	{!!Form::close()!!}

@stop

@section('footer')


@stop