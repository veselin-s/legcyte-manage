<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	public function isAdminRegistered(){
		$email=[];
		$ret=false;
		$user=User::all();
		$emails=[];
		
		//getting all users' emails
		for($i=0;$i<count($user);$i++){
			$emails[$i]=$user[$i]->email;
			
		}
		
		//if emails are more than 1 checking if they equal to these values
		if(count($emails)>1){
			if(in_array('sargr86@gmail.com',$emails) && in_array('hasmik@deemcommunications.com',$emails)){
				
				$ret=true;
			}
		}

		return $ret;
		
	}

}
