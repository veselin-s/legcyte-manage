<?php

//PARENTS
Route::get('/','SubjectsController@index');
Route::post('parent/add','SubjectsController@store');
Route::post('parent/remove', 'SubjectsController@destroy');
Route::post('parent/update', 'SubjectsController@update');

//CHILDREN
Route::post('child/add','SubjectsController@storeChild');
Route::post('child/remove', 'SubjectsController@destroy');
Route::post('child/update', 'SubjectsController@update');
Route::get('children/get', 'SubjectsController@getChildrenOfParent');


//ORPHANS
Route::get('orphans/get', 'SubjectsController@getOrphans');



//Authentication
Route::controllers([
	'auth'=>'Auth\AuthController',
	'password'=>'Auth\PasswordController',
]);






