<?php namespace App\Http\Middleware;

use Closure;
use App\User;

class NoRegisterIfNotManager {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		
		$obj=new User;
	
		//if 2 admins registered (checking by their emails) redirecting back for no further registering on this site 
		if($obj->isAdminRegistered()) return redirect('works');
		
		
		return $next($request);
	}

}
