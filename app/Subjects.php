<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Subjects extends Model {

	protected  $fillable=['Id','Name','ParentId'];
	public $timestamps=false;


	public static function getChildren($id){
		return Subjects::where(['ParentId'=>$id])->get();
	}

}
